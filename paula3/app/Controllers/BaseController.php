<?php

class BaseController{
	
	public function __construct(){
		
		$this->helpers = new Helpers;
		$this->view = new View;
		$this->model = new Model;
		$this->extender = new Extender;
	}
	

	
	
}

<?php

class html_forms{
	
	
	public function open($action,$method,$class){
		echo '<form class="'.$class.'" action="'.$action.'" method="'.$method.'">';
	}
	public function close(){
		echo '</form>';
	}
	public function input($type,$name,$placeholder){
		echo '<input type="'.$type.'" name="'.$name.'" placeholder="'.$placeholder.'">';
	}
	public function submit($value){
		echo '<input type="submit" value="'.$value.'">';
	}
	
}

<?php

class input_cleaner{
	
	public function clean_input_db($input){
		
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $input);

		
	}
	
}

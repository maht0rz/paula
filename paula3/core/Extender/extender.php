<?php

class Extender{
	
	public function extend($extension_name){
		
		require (dirname(dirname(__FILE__)).'/../app/Extensions/'.$extension_name.'.php');
		
		if(class_exists($extension_name)){
			$this->$extension_name = new $extension_name;
			if(method_exists($this->$extension_name, 'load')){
				
			}
		}
		
	}
	
}

<?php
class Model{
	
    function __construct(){
    	
	}

	public function load($location){
		$model =  (dirname(dirname(__FILE__))."/../app/Models/".$location.".php");
	require($model);
	$this->$location = new $location;
	}
	
	public function connect($host,$dbuser,$dbpass,$dbname){
		
		$this->connector = mysqli_connect($host,$dbuser,$dbpass,$dbname);
		return $this;
	}
	
	public function table($table){
	
	$this->table = $table;
		
		return $this;
	}
	public function all(){
	
	$this->ar = mysqli_query($this->connector,"SELECT * FROM ".$this->table);
		$this->result = mysqli_fetch_array($this->ar);
		
		return $this;	
		
	}
	
	public function where($what,$how,$whateq){
	$where = $what." ".$how." "."'".$whateq."'";
	$this->where = $where;
	$this->whereif = true;	
		return $this;	
		
	}
	
	public function insert($what){
		
		
			
			$keys = array_keys($what);
			$values = array_values($what);
			
			$finalkeys="";
			foreach ($keys as $keys) {
				
			$finalkeys = $finalkeys.",".$keys;
				
			}
			$finalkeys = ltrim ($finalkeys,',');
			#echo $finalkeys;
			
			$finalvalues = "";
			foreach ($values as $values) {
				
			$finalvalues = $finalvalues.",'".$values."'";
				
			}
			$finalvalues = ltrim ($finalvalues,',');
			#echo $finalvalues;
			
			#var_dump($this->connector,"INSERT INTO ".$this->table." (".$finalkeys.") VALUES (".$finalvalues.")");
			
		
		$this->ar = mysqli_query($this->connector,"INSERT INTO ".$this->table." (".$finalkeys.") VALUES (".$finalvalues.")");
		
		return $this;	
		
	}
	
		public function update($what){
		
		
			
			$keys = array_keys($what);
			$values = array_values($what);
			
			$finalkeys="";
			foreach ($keys as $keys) {
				
			$finalkeys = $finalkeys.",".$keys;
				
			}
			$finalkeys = ltrim ($finalkeys,',');
			#echo $finalkeys;
			
			$finalvalues = "";
			foreach ($values as $values) {
				
			$finalvalues = $finalvalues.",'".$values."'";
				
			}
			$finalvalues = ltrim ($finalvalues,',');
			#echo $finalvalues;
			
			#var_dump($this->connector,"INSERT INTO ".$this->table." (".$finalkeys.") VALUES (".$finalvalues.")");
			
		
		$this->ar = mysqli_query($this->connector,"INSERT INTO ".$this->table." (".$finalkeys.") VALUES (".$finalvalues.")");
		
		return $this;	
		
	}
	
	
	
	public function get(){
		
		if(isset($this->whereif)){
			$this->ar = mysqli_query($this->connector,"SELECT * FROM ".$this->table." WHERE ".$this->where);
		#$row = mysqli_fetch_assoc($this->ar);
		 #$row = $this->ar->fetch_assoc();
		 
		
		$this->arraytopush = array();
		
		while ($data = mysqli_fetch_array($this->ar, MYSQLI_NUM)) {
			
		array_push($this->arraytopush, $data);
		}
		 
		 $this->result = $this->arraytopush;
		 
		}else{
			
		$this->ar = mysqli_query($this->connector,"SELECT * FROM ".$this->table);
	
	
			$this->arraytopush = array();
		
		while ($data = mysqli_fetch_array($this->ar, MYSQLI_NUM)) {
			
		array_push($this->arraytopush, $data);
		}
		 
		 $this->result = $this->arraytopush;
		}
		
		
		return $this;
	}
	
	public function pluck($plucker){
			
		if(isset($this->whereif)){
			$this->ar = mysqli_query($this->connector,"SELECT ".$plucker." FROM ".$this->table." WHERE ".$this->where);
		}else{
			
		$this->ar = mysqli_query($this->connector,"SELECT ".$plucker." FROM ".$this->table);
	
		}
		
		
		$this->result = mysqli_fetch_row($this->ar);
		return $this;
		
	}
	
}
<?php

class Loader{
	
	public function load_core(){
		
		require_once '../app/Config/app.php';
		require_once '../core/Errors/handler.php';
		
		require_once '../core/Extender/extender.php';
		require_once '../core/Helpers/helpers.php';
		require_once '../core/View/viewer.php';
		require_once '../core/Model/modeler.php';
		
		require_once '../core/Routing/router.php';
		
	}
	
	public function load_extensions(){
	
		
	}
	
	public function load(){
		
		$this->load_core();
		$this->load_extensions();
		
	}
	
	
}


$Paula = new Loader;

$Paula->load();

<?php

class Router{
	
	public function __construct(){
		
		$this->parent = new Loader;
		
	}
	
	public function __destruct(){
		/*
		echo "<br><br><span style='padding-right:7px;'>Memory usage: ".(memory_get_peak_usage(true) /1024 / 1024)." MB</span>";
echo "|<span style='padding-left:7px;'>Generated in: ".(round((microtime(true) - $GLOBALS['start']) * 1000, 2))."ms<span>";
		*/
	}
	
	public function get_config(){
		
		$this->localhost = Config_app::$localhost;
		
		$this->local_folder = Config_app::$local_folder;
		
	}
	
	public function set_url(){
		
		$this->url = $_SERVER['REQUEST_URI'];
		
		$this->exploded_url = explode('/',$this->url);
		
		if($this->localhost){

			$this->new_url = array_slice($this->exploded_url,$this->local_folder+1);

		}else{
			
			$this->new_url = $this->exploded_url;
			
					
		}
			
		
		
		
	}
	
	
	
	public function find(){
		if(file_exists('../app/Controllers/BaseController.php')){
			require_once '../app/Controllers/BaseController.php';
		}else{
			trigger_error("BaseController file not found!");
		}
		if(file_exists('../app/Models/BaseModel.php')){
			require_once '../app/Models/BaseModel.php';
		}else{
			trigger_error("BaseModel file not found!");
		}
		$this->url_lenght = sizeof($this->new_url);
		
		switch ($this->url_lenght) {
			case '1':
				$file = '../app/Controllers/'.$this->new_url[0].'.php';
					if(file_exists($file)){
						require_once $file;
						
						if(class_exists($this->new_url[0])){
								if(method_exists($this->new_url[0], 'index')){
									$class = $this->new_url[0];
									$method = 'index';
									
									$obj = new $class;
									
									$m = new ReflectionMethod($class, $method);
									$num = $m->getNumberOfParameters();
																	
									if($num > 0){
										
											trigger_error("Route not found!");
										
										
									}else{
										
											$obj->$method();
										
									}
									
									
									
								}else{
									trigger_error("Method not found!");	
								}
							}else{
								trigger_error("Class not found!");
							}
						
						
						
						
						
						
					}else if(file_exists('../app/Controllers/IndexController.php')){
							require_once '../app/Controllers/IndexController.php';
							
							if(class_exists('IndexController')){
								if(method_exists('IndexController', 'index')){
									$class = 'IndexController';
									$method = 'index';
									
									$obj = new $class;
									
									$m = new ReflectionMethod($class, $method);
									$num = $m->getNumberOfParameters();
																	
									if($num > 0){
										if($this->new_url[0] != ""){
											$obj->$method($this->new_url);
										}else{
											trigger_error("Route not found!");
										}
										
									}else{
										if($this->new_url[0] == ""){
											$obj->$method();
										}else{
											
											trigger_error("Route not found!");
										}
									}
									
									
									
								}else{
									
									trigger_error("Method not found!");	
								}
							}else{
							
								trigger_error("Class not found!");
							}
							
						}else{
								
							trigger_error("File of class not found!");
						}
					
				
				break;
			case '2':
			
					$file = '../app/Controllers/'.$this->new_url[0].'.php';
					if(file_exists($file)){
						
						require_once $file;
						
						if(class_exists($this->new_url[0])){
								if(method_exists($this->new_url[0], $this->new_url[1])){
									$class = $this->new_url[0];
									$method = $this->new_url[1];
									
									$obj = new $class;
									
									$m = new ReflectionMethod($class, $method);
									$num = $m->getNumberOfParameters();
																	
									if($num > 0){
										
											trigger_error("Route not found!");
										
										
									}else{
										
											$obj->$method();
										
									}
									
									
									
								}else{
										if(method_exists($this->new_url[0], 'index')){
									$class = $this->new_url[0];
									$method = 'index';
									
									$obj = new $class;
									
									$m = new ReflectionMethod($class, $method);
									$num = $m->getNumberOfParameters();
																	
									if($num > 0){
										
											$obj->$method($this->new_url);
										
										
									}else{
									
											trigger_error("Route not found!");
										
									}
									
									
									
								}else{
									trigger_error("Method not found!");	
									
								}
									
							
									
								}
								
							}else{
								trigger_error("Class not found!");
							}
						
						
					}else{
						trigger_error("File of class not found!");
					}
			
				break;
			
			
			default:
				
					$file = '../app/Controllers/'.$this->new_url[0].'.php';
					if(file_exists($file)){
						require_once $file;
						
						if(class_exists($this->new_url[0])){
								if(method_exists($this->new_url[0], $this->new_url[1])){
									$class = $this->new_url[0];
									$method = $this->new_url[1];
									
									$obj = new $class;
									
									$m = new ReflectionMethod($class, $method);
									$num = $m->getNumberOfParameters();
																	
									if($num > 0){
										$obj->$method($this->new_url);
											
										
										
									}else{
										trigger_error("Route not found!");
											
										
									}
									
									
									
								}else{
									trigger_error("Method not found!");	
								}
							}else{
								trigger_error("Class not found!");
							}
						
						
					}
				
				
				
				break;
		}
		
	}
	
	public function load(){
		
		$this->get_config();
		
		$this->set_url();
		
		
		
		$this->find();
		
	}
	
}

$this->router = new Router;
$this->router->load();

